public class Widerstandsnetzwerk {

	public static void main(String[] args) {
		
		// Aufgabe1    (5P)
		//    a) Deklariern Sie Variablen f�r die Speicherung von 3 Widerstandswerten (z.B. 1.2 Ohm) und
		//       initialisieren Sie mit den Werten 1.2, 2.7 und 8.2.   (3 P)
		//
		//    b) Erstellen Sie ein commit mit dem Kommentar  "Variablendeklaration abgeschlossen."  (2P)
		//
		// Hinweis:  Ohm ist die Einheit, mit der man Widerst�nde misst.

		double widerstand1 = 1.2;
		double widerstand2 = 2.7;
		double widerstand3 = 8.2; 
		
		//Aufgabe2   (7P)
		//   a) Berechnen Sie den Ersatzwiderstand f�r die folgenden F�lle und 
		//      speichern Sie die Ergebnisse jeweils in einer geeigneten Variable.
		//          - die Widerst�nde sind in Reihe geschaltet.   (2P)
		//          - die Widerst�nde sind parallel geschaltet.   (3P)
		//
		//      Hinweis:  
		//         Berechnungsregel f�r Reihen-Schaltung:
		//               Der Gesamtwiderstandswert berechnet sich aus der Summe der Einzelwiderst�nde.     
		//         Berechungsregel f�r Parallel-Schaltung: 
		//              Der Kehrwert des Gesamtwiderstandes berechnet sich aus der Summe der Kehrwerte der Einzelwiderst�nde.
		//
		//   b) Erstellen Sie ein commit mit dem Kommentar  "Berechnungen f�r die Aufgabe 2 implementiert."   (2P)
		//

	
		// L�sung f�r a)
		double wertReihenschaltung = widerstand1 + widerstand2 + widerstand3;
		
		// L�sung f�r b)
		double sumKehrwerte = (1.0/widerstand1 + 1.0/widerstand2 + 1.0/widerstand3);
		double wertParallelschaltung = 1.0 /sumKehrwerte;

		/*    weitere L�sungsm�glichkeiten f�r b):
				double wertParallelschaltung = 1.0/(1.0/widerstand1+ 1.0/widerstand2 +1.0/widerstand3);
				double wertParallelschaltung = widerstand1 *widerstand2 * widerstand3 / 
					                           widerstand2 * widerstand3 + widerstand1 * widerstand3 + widerstand1 * widerstand2;
		*/	
		
		// Aufgabe3   (6P)
		//   a) Geben Sie die Ergebnisse aus der Aufgbe2 wie folgt aus.   (4P)
		//    
		//        Ersatzwiderstand der Schaltung
		//                 - Reihenschaltung:    12.10 Ohm	
		//                 - Parallelschaltung:   0.75 Ohm
		//
		//  b) Erstellen Sie ein commit mit dem Komentar  "Berechnungen f�r die Aufgabe 2 implementiert." (2p)
		
		System.out.println("Ersatzwiderstand der Schaltung");
		System.out.format("        - Reihenschaltung:   %7.2f Ohm%n", wertReihenschaltung);
		System.out.format("        - Parallelschaltung: %7.2f Ohm%n", wertParallelschaltung);
	}

}