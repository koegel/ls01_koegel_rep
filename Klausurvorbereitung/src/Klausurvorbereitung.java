
public class Klausurvorbereitung {

	public static void main(String[] args) {
		// Kommentar
		
		/* dies ist 
		 * ein 
		 * mehrzeiliger
		 * Kommentar
		 */
		
		//Variable deklariert: vom Typ String, mit Namen text
		//Variable gleichzeitig initialisiert: Der Variablen wird ein Wert zugewisesn
		String text = "trallalla";
		System.out.println("Zeile 16 " + text);
		
		//Integer: ganze Zahlen 
		int zahl1 = 345, zahl2 = 5;  //camel case   Java ist case sensitive 
		int ergebnis = zahl1+zahl2;
		System.out.println(ergebnis);
		

		
		text = "ein neuer Text"; 
		System.out.println("Zeile 22 " + text);
		
		
		System.out.println(127 + " trallalla");
		
		//String: Datentyp: Zeichenkette / W�rter, nicht zum Rechnen
		
		System.out.println("127");
		
		
		

	}

}
