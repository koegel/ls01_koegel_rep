/* Operatoren.java
   Uebung zu Operatoren in Java
*/
public class OperatorenLoeser {
  public static void main(String[] args) {
    /* 1. Deklarieren Sie zwei Ganzzahlen.*/
    int zahl1;
    int zahl2;
    
    System.out.println("UEBUNG ZU OPERATOREN IN JAVA");
    System.out.println();
    
    /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
    und geben Sie sie auf dem Bildschirm aus. */
    zahl1 = 75;
    zahl2 = 23;
    System.out.println("Ganzzahl a ist gleich " + zahl1 + " und Ganzzahl b ist "+ zahl2);
    /* 3. Addieren Sie die Ganzzahlen
    und geben Sie das Ergebnis auf dem Bildschirm aus. */
    int summe = zahl1+zahl2;
    System.out.println("zahl1 + zahl2 = "+ (zahl1+zahl2));
    System.out.println();
    /* 4. Wenden Sie *alle anderen* arithmetischen Operatoren auf die
    Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
    Bildschirm aus. */    
    System.out.println("zahl1 % zahl2 = "+ (zahl1%zahl2));
    System.out.println("zahl1 / zahl2 = "+ ((double)zahl1/zahl2));
    System.out.println("zahl1 * zahl2 = "+ (zahl1*zahl2));
    System.out.println("zahl1 - zahl2 = "+ (zahl1-zahl2));
    System.out.println();
    
    /* 5. Ueberpruefen Sie, ob die beiden Ganzzahlen gleich sind
    und geben Sie das Ergebnis auf dem Bildschirm aus. */
    System.out.println("Ist zahl1 = zahl2?  " +(zahl1==zahl2));
    System.out.println();
    
    /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
    und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */    
    System.out.println("Ist zahl1 <= zahl2? " +(zahl1<=zahl2));
    System.out.println("Ist zahl1 >= zahl2? " +(zahl1>=zahl2));
    System.out.println("Ist zahl1 < zahl2?  " +(zahl1<zahl2));
    System.out.println("Ist zahl1 > zahl2?  " +(zahl1>zahl2));
    System.out.println();
    /* 7. Ueberpruefen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
    und geben Sie das Ergebnis auf dem Bildschirm aus. 
    
    Tipp: Auch das geht nur mit Operatoren!
    */    
    boolean aInIntervall = (zahl1>=0) && (zahl1<=50);
    boolean bInIntervall = (zahl2>=0) && (zahl2<=50);
    System.out.println("Ist zahl1 in [0;50]? " + aInIntervall);
    System.out.println("Ist zahl2 in [0;50]? " + bInIntervall);
    System.out.println();
  } //Ende von main
} // Ende von Operatoren
