
public class Konsolenausgabe1 {

	public static void main(String[] args) {
		
		//Aufgabe 1
		
		System.out.println("\nAufgabe1 \n");
		System.out.println("Das ist ein \"Beispielsatz\". ");
		System.out.print("Und ein zweiter. (nicht mehr) In der selben Zeile.\n\n\n");
		
		
		//Aufgabe 2
		
		System.out.println("Aufgabe2 \n");
		System.out.printf("%6s\n", "*");
		System.out.printf("%7s\n", "***");
		System.out.printf("%8s\n", "*****");
		System.out.printf("%9s\n", "*******");
		System.out.printf("%10s\n","*********");
		System.out.printf("%8s\n", "***********");
		System.out.printf("%7s\n", "***");
		System.out.printf("%7s\n\n\n\n\n", "***");
		
		//Aufgabe 3
		
		System.out.println("Aufgabe3 \n");
		//15 Stellen f�r die Ausgabe, davon 2 Nachkommastellen
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 100000000.551;
		double e = 97.34;
		
		System.out.printf("%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e); 
		
		
		
		
		System.out.println("\n\n Blatt 2 - Aufgabe 3 Temperaturtabelle \n");
		
		// %-12s linksb�ndig, 12 Stellen, String
		System.out.printf( "%-12s| %10s\n", "Fahrenheit", "Celsius" );
		System.out.println("------------------------");
		
		// %-12d = linksb�ndig, 12 Stellen, ganze Zahl
		// %10.2f = 10 Stellen, davon 2 Nachkomma
		System.out.printf( "%-12d| %10.2f\n", -20, -28.89 );
		System.out.printf( "%-12d| %10.2f\n", -10, -23.33 );
		
		// bei Ganzzahlen:  %+ o. %-   Vorzeichen wird mit ausgegeben
		System.out.printf( "%+-12d| %10.2f\n", +0, -17.78 );
		System.out.printf( "%+-12d| %10.2f\n", +20, -6.67 );
		System.out.printf( "%+-12d| %10.2f\n", +30, -1.11 );
		
		
		System.out.printf( "Schuelerloesung");
		double x = 22.4234234;
		System.out.printf( "\n%.2f " , x);
		x = 111.2222;
		System.out.printf("\n%.2f", x);
		x = 4.0;
		System.out.printf("\n%.2f", x);
		x = 1000000.551;
		System.out.printf("\n%.2f", x);
		x = 97.34;
		System.out.printf("\n%.2f", x);
		
	}

}
