

public class Temperaturtabelle {

	public static void main(String[] args) {
		
		System.out.println("Aufgabe 3 Temperaturtabelle \n\n");
		
		// %-12s linksbŁndig, 12 Stellen, String
		System.out.printf( "%-12s| %10s\n", "Fahrenheit", "Celsius" );
		System.out.println("------------------------");
		
		// %-12d = linksbŁndig, 12 Stellen, ganze Zahl
		// %10.2f = 10 Stellen, davon 2 Nachkomma
		System.out.printf( "%-12d| %10.2f\n", -20, -28.89 );
		System.out.printf( "%-12d| %10.2f\n", -10, -23.33 );
		
		// bei Ganzzahlen:  %+ o. %-   Vorzeichen wird mit ausgegeben
		System.out.printf( "%+-12d| %10.2f\n", +0, -17.78 );
		System.out.printf( "%+-12d| %10.2f\n", +20, -6.67 );
		System.out.printf( "%+-12d| %10.2f\n", +30, -1.11 );

	}


}