// Klasse Scanner muss aus dem package java.util importiert werden
import java.util.Scanner;

public class TastaturEingaben {

	public static void main(String[] args) {
		
		// Ein Objekt der Klasse Scanner wird erstellt (namens sc)
		Scanner sc = new Scanner (System.in);
		String name;
		
		System.out.println("Name ?");
		
		
		name = sc.nextLine();
		
		System.out.println(name);
		
		sc.close();
	}

}
