public class Quadrieren {
   
	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert fuer x festlegen:
		// ===========================
		System.out.println("Dieses Programm berechnet die Quadratzahl x^2");
		System.out.println("---------------------------------------------");
		double x = 5;
				
		// (V) Verarbeitung
		// Quadratzahl berechnen:
		// ================================
		double ergebnis= x * x;

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("x = %.2f und x^2= %.2f\n", x, ergebnis);
	}	
}
