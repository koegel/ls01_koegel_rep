/*
 * Erzeugen Sie bitte ein lauffähiges Programm. Gegeben ist das folgende Hauptprogramm. Lagern Sie 

    - den Programmhinweis,
	- den Ausgabeteil,
    - den Verarbeitungsteil,
    - und den Eingabeteil 

jeweils in eine Methode aus. Rufen Sie die Methoden auf. Verwenden Sie keine globalen Variablen. Achten Sie bitte auf die angegebene Reihenfolge, die nach Schwierigkeitsgrad sortiert ist.
(Tipp: die Methode kann auch mehrmals aufgerufen werden.)
 * 
 * 
 */




import java.util.Scanner;
public class KlausuraufgabeModularisierungLoesung {

	public static void main(String[] args) {
		
		double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
		Scanner myScanner = new Scanner(System.in);
		
		//1. Programminweis
		programmhinweis();						// 1P
		
		//4. Eingabe
		zahl1 = eingabe("1. Zahl", myScanner);	// 3 P
		zahl2 = eingabe("2. Zahl", myScanner);
		
		//3. Verarbeitung
		erg = verarbeitung(zahl1, zahl2);    // 2P
		
		//2. Ausgabe
		ausgabe(zahl1, zahl2, erg);			// 2P
		
	}
	
	public static void programmhinweis() {		// 2 P
		System.out.println("Hinweis: ");
		System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");
	}
	
	public static void ausgabe(double zahl1, double zahl2, double erg) {    // 3 P
		System.out.println("Ergebnis der Multiplikation: ");
		System.out.printf("%.2f * %.2f = %.2f%n", zahl1, zahl2, erg);
	}
	
	public static double verarbeitung (double zahl1, double zahl2) {   // 3 P
		return (zahl1 * zahl2);
	}
	
	public static double eingabe (String text, Scanner myScanner) {   // 4 P 
		System.out.println(text + " eingeben bitte ");
		return myScanner.nextDouble();
	}
}
