public class Modularisierung1 {

	
	public static void main(String[] args){
		int zahl=333;
		System.out.println (zahl + " in main Methode");
		Modularisierung1.aendern();
		System.out.println (zahl + " in main Methode");
		System.out.println ("\n\n\n");
		
		System.out.println ("JETZT WIRKLICH �NDERN: ");
		System.out.println (zahl + " in main Methode");
		zahl = Modularisierung1.wirklichAendern(zahl);
		System.out.println ("ge�nderte Zahl: " +zahl + " (nun in Main Methode)");
	}
	
	
	//lokale Variable, hat keinen Einfluss auf die Variable in der Main Methode
	public static void aendern() {
		int zahl = 111;
		System.out.println("In der Methode 'aendern': " + zahl );
	}

	//bekommt Variable aus Main-Methode �bergeben und gibt die ge�nderte Variable zur�ck
	public static int wirklichAendern(int a) {
		int zahl = 222;
		System.out.println("In der Methode 'wirklichAendern': " + zahl );
		return zahl;
	}
	
}
