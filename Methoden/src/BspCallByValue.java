import java.util.Scanner;


public class BspCallByValue {



	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int wahl, z;
		
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		z = sc.nextInt();
		
		System.out.println("Wollen Sie diese Zahl\n");
		System.out.println("\t 1.) verdoppeln\n\t 2.) halbieren\n\nIhre Wahl: ");
		wahl = sc.nextInt();
		
		switch(wahl) {
			case 1 : BspCallByValue.verdoppeln(z);
			break;
			case 2 : BspCallByValue.halbieren(z);
			break;
			default : System.out.println("Unbekannte Eingabe\n");
		}
		
		sc.close();
	}
	
	public static void halbieren(int u_zahl) {
		u_zahl = u_zahl / 2;
		System.out.println("Halbiert : " + u_zahl);
	}

	public static void verdoppeln(int u_zahl) {
		u_zahl = u_zahl * 2;
		System.out.println("Verdoppelt : " + u_zahl);
	}
	
	
}