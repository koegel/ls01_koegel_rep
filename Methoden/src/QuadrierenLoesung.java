import java.util.Scanner;

public class QuadrierenLoesung {
   
	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert fuer x festlegen:
		// ===========================
		title();
		double x = eingabe();
		double ergebnis;

		// (V) Verarbeitung
		// Quadratzahl berechnen:
		// ================================
		ergebnis = verarbeitung(x);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		ausgabe(x,ergebnis);
	}
	
	public static void title() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x^2");
		System.out.println("---------------------------------------------");
	}
	
	public static double eingabe() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein: ");
		double zahl = scanner.nextDouble();
		scanner.close();
		return zahl;
	}

	public static double verarbeitung(double x) {
		return x * x;
	}
	
	public static void ausgabe(double x, double erg) {
		System.out.printf("x = %.2f und x^2= %.2f\n", x, erg);
	
	}
	 
  
}



