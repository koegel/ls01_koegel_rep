import java.util.Scanner;

public class MeinMittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x, y, m = 0.0;
      
      x = MeinMittelwert.eingabe();
      y = MeinMittelwert.eingabe();
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = MeinMittelwert.verarbeitung(x, y, m);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      MeinMittelwert.ausgabe(x, y, m);
     
   } //end of main
   
  
   //Methode f�r die Eingabe
   
   //-----------------------------------------------------------
   // ACHTUNG, FUNKTIONIERT BEI DER 2. EINGABE NICHT MEHR !!!!!!
   //-----------------------------------------------------------
   public static double eingabe () {
	   Scanner sc = new Scanner(System.in);
	   double a;
	   
	   System.out.print("Bitte geben Sie eine Zahl ein: ");
	   a = sc.nextDouble();
	   
	   sc.close();
	  
	   return a;    
   } //end of eingabe
   
   //Methode f�r die Verarbeitung
   public static double verarbeitung (double x, double y, double m) {
	  m = (x + y) / 2;
	  return m;
   } //end of verarbeitung
   
   // Methode f�r die Ausgabe
   public static void ausgabe (double x, double y, double m) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   } //end of ausgabe
   
   
}
