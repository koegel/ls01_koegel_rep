/*
 * Erzeugen Sie bitte ein lauffähiges Programm. Gegeben ist das folgende Hauptprogramm. Lagern Sie 

    - den Programmhinweis,
	- den Ausgabeteil,
    - den Verarbeitungsteil,
    - und den Eingabeteil 

jeweils in eine Methode aus. Rufen Sie die Methoden auf. Verwenden Sie keine globalen Variablen. Achten Sie bitte auf die angegebene Reihenfolge, die nach Schwierigkeitsgrad sortiert ist.
(Tipp: die Methode kann auch mehrmals aufgerufen werden.)
 * 
 * 
 */




import java.util.Scanner;
public class KlausuraufgabeModularisierung {

	public static void main(String[] args) {
		
		double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
		Scanner myScanner = new Scanner(System.in);
		
		//1. Programminweis
		System.out.println("Hinweis: ");
		System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");
		
		//4. Eingabe
		System.out.println(" 1. Zahl: ");
		zahl1 = myScanner.nextDouble();
		System.out.println(" 2. Zahl: ");
		zahl2 = myScanner.nextDouble();
		
		//3. Verarbeitung
		erg = zahl1 * zahl2;
		
		//2. Ausgabe
		System.out.println("Ergebnis der Multiplikation: ");
		System.out.printf("%.2f * %.2f = %.2f%n", zahl1, zahl2, erg);
		
	}
}
