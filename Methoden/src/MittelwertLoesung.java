import java.util.Scanner;

public class MittelwertLoesung {

   public static void main(String[] args) {

      // (E) "Eingabe"
     
	  double x;
      double y;
      double m;
      
      Scanner scan = new Scanner(System.in);
      
      System.out.print("Bitte geben Sie eine Zahl ein: ");
      x = scan.nextDouble();
  
      System.out.print("Bitte geben Sie eine zweite Zahl ein: ");
      y = scan.nextDouble();
      
      
      // (V) Verarbeitung

      m = berechneMittelwert(x, y);
      
      // (A) Ausgabe

      ausgabe (x, y, m);
            
      scan.close();
   }
   
   
   // Methode Verarbeitung
   public static double berechneMittelwert(double x1, double x2) {
      
      double mittelwert;
      
      mittelwert = (x1 + x2) / 2.0;
      
      return mittelwert;
   }
   
   
   // Methode Ausgabe
   public static void ausgabe (double x, double y, double m) {
	   
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
	   
   }
   
}
