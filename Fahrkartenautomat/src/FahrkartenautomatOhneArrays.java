import java.util.Scanner;

class FahrkartenautomatOhneArrays {

	/**
	 * Erfasst die Bestellung
	 * 
	 * @return Gesamter Ticketpreis
	 */
	public static double fahrkartenbestellungErfassen() {
		int anzahlTickets;
		double ticketPreis = 0.0;
		Scanner tastatur = new Scanner(System.in);

		// Aufgabe 4.7 - Menu zur Auswahl von Fahrkartentypen um wiederholte Eingabe erweitert

		double preisAllerTickets = 0.0;
		boolean auswahlBeenden = false;

		while (true) {

			int auswahlTickets = 0;
			boolean korrekteEingabe = false;

			System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
			System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
			System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
			System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
			System.out.println("Bezahlen (9)\n");

			while (korrekteEingabe == false) {
				System.out.print("Ihre Wahl: ");
				auswahlTickets = tastatur.nextInt();
				if (auswahlTickets >= 1 && auswahlTickets <= 3) {
					korrekteEingabe = true;
				} 
				else if (auswahlTickets == 9) {
					korrekteEingabe = true;
					auswahlBeenden = true;
				} 
				else {
					System.out.println(" >>falsche Eingabe<< ");
				}
			}

			if (auswahlBeenden) {
				break;
			}

			if (auswahlTickets == 1) {
				ticketPreis = 2.9;
			} 
			else if (auswahlTickets == 2) {
				ticketPreis = 8.6;
			} 
			else if (auswahlTickets == 3) {
				ticketPreis = 23.5;
			}

			korrekteEingabe = false;
			anzahlTickets = 0;

			while (korrekteEingabe == false) {
				System.out.print("Anzahl der Tickets: ");
				anzahlTickets = tastatur.nextInt();

				if (anzahlTickets >= 1 && anzahlTickets <= 10) { // Nur Eingaben von 1 bis 10 sind erlaubt
					korrekteEingabe = true;
				} else {
					System.out.println(" >> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.\n");
				}

			}

			preisAllerTickets = preisAllerTickets + ticketPreis * anzahlTickets;

			System.out.format("%nZwischensumme: %4.2f � %n%n", preisAllerTickets);

		}

		return preisAllerTickets;
	}

	/**
	 * Erh�lt den zu zahlenden Betrag und fragt den User ab, bis er den Betrag
	 * bezahlt hat
	 * 
	 * @param zuZahlenderBetrag, Betrag der zuzahlen ist
	 * @return R�ckgeld, wie viel �berbezahlt wurde
	 */
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfenemuenze;
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.format("Noch zu zahlen: %4.2f � %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfenemuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfenemuenze;
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	/**
	 * Gibt animiert die Meldung aus, das bezahlt wurde
	 */
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	/**
	 * Erh�lt dem R�ckgabebetrag und zahlt diesen in passenden M�nzen aus
	 */
	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {
			System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-M�zen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}
	}

	/**
	 * Main Methode, die alles aufruft
	 * 
	 * @param args, Startargumente, werden ignoriert
	 */
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double rueckgabebetrag;

		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rueckgabebetrag);

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}

}