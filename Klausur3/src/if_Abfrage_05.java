
/**
*
* if-Abfrage-13 Auswahl_frage04
*
* @version 1.0 from 22.03.2022
* @author 
*/

public class if_Abfrage_05 {

public static void main(String[] args) {
  
  int x = 7, y = 3, z = 9;
  
  if ((z+y) <= 2 * x && x<y || y > 0){   // true
    x = z-3;                             //x = 6
    y = --x;                            // y = 5
    //System.out.println(y);
  }
  else{
    x = 2*y-3;
    y = z-2*x;
    z+=1;
   }
  System.out.println(""+x+y+z);   //   Konkatenation von Strings 
}
}