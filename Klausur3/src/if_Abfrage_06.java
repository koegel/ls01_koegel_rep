

/**
*
* if-Abfrage-11 Auswahl_03
*
* @version 1.0 from 22.03.2022
* @author 
*/

public class if_Abfrage_06 {

public static void main(String[] args) {
   int x = 7, y = 3, z = 9;
  if (x<y || (z-y) <= x){
    x = z-3;                       // x = ??
    y = x++;                       // was passiert?
   // System.out.println("y: " + y);  // y = ??
   // System.out.println("x: " + x);  // x = ?? 
    ++z;                             // z = ?? 
    }
  
  else{
    x = 2+y;
    y = z-x;
    z--;
    }
  
   x++;       // x = ??
  
  System.out.println(x+y+z); //  ??
}
}