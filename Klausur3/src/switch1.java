/**
  *
  * description
  *
  * @version 1.0 from 23.03.2022
  * @author 
  */

public class switch1 {

  public static void main(String[] args) {
    int a = 7;
    char c = 'a'; // oder 'b' oder 'x'
    switch (c) {
      case 'a':
        while (a >= 1)
          a = a - 2;
          a = 3 * a;
        break;
      case 'b':
        for (int i = 10; i > 5; i--)
          a = a + 3;
        break;
      default:
        
        a = a % 3;
        break;
        
    } // switch
    
    
    if (a > 15)
      a--;
      
    else
      a++;
    
    System.out.println("a: " + a);

  }
}
