
/**
  *
  * if-Abfrage-02 Auswahl_frage02

  */


public class if_Abfrage_02 {

  public static void main(String[] args) {

      int x = 7;
      int y = 5;
      int z = 3;
      
      //( y++ > 5) -> true oder false ? 
      
      if (x<5 || y++ > 5)       // false || false  , y = 6       
        z = z+2;
          
      System.out.println("y: " + y);  // y = 6
      
      if (y<6 || z <=5)  // false || true -> true 
       x = 5;
      else if (z<3)
        y = 3;
        else 
          y = 2;
       System.out.println( x+""+y+""+z);   
      
        } // end of if-else
      } // end of if-else
  /**
              x        y         z
              7        5         3
    19 false           6
    21 true
    22        5
    28 SOP 563

  **/