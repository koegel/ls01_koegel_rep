


  /**
    *
    * if-Abfrage-03 Auswahl_frage03
    *
    * @version 1.0 from 22.03.2022
    * @author 
    */

  public class if_abfrage_03 {

    public static void main(String[] args) {
      
      int x = 9;
      int y = 7;
      int z = 2;
      
      if ( x % 5 < 5 || ++y > 7 ) {     // true   
        z = z-1;                        // z = ??
      System.out.println("y = " + y);}  // y = ??
     
      else 
        z = z+1;
      
      
      if (y+z <9 && z++ <3) {            // true && true
        x = x+2;                            // x = 11
        System.out.println("z = " + z);}    //z = 2
      
      else {
        x = x-2;             
      } 
      
      z++;          // z = 3
      System.out.println(x+""+y+""+z);    //
    

  }
  }