/**
  *
  * if-Abfrage-01 Auswahl_frage01
  *

  */

public class if_Abfrage_01 {

  public static void main(String[] args) {
    int j = 4;
    int i = 3;
    int result = 0;

    if(j<5 && i>5)         // false
      result = j+i++;
    
    if(j<5 || i<5)         // true  
      result = j-i++;     //  zuerst: j-i in result. DANACH i erhoehen
    
    if(i>3)               // true
      result = 2*j + i++;  //s.o. 
      
    System.out.println("result " + result);
    System.out.println("i = " + i);
  }
}
