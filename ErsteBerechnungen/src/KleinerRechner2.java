
//Scanner Klasse muss importiert werden, um Nutzereingaben einlesen zu k�nnen
import java.util.Scanner;


public class KleinerRechner2 {
	
	public static void main(String[] args) {
		
		//wir erstellen einen eigenen Scanner, Namens "sc"
		//der Name kann beliebig gew�hlt werden
		Scanner sc = new Scanner (System.in);
		
		//Deklaration von 3 ganzzahligen Variablen 
		int zahl1, zahl2, ergebnis;
		
		System.out.println("Geben Sie die erste ganzzahlige Zahl ein");
		zahl1 = sc.nextInt();  //mit Hilfe des Scanners "sc" wird ein Integer-Wert eingelesen
		
		System.out.println("Geben Sie die zweite ganzzahlige Zahl ein");
		zahl2 = sc.nextInt();
		
		//das Ergebnis der Berechnung auf der rechten Seite wird der Variablen "ergebnis" zugewiesen
		ergebnis = zahl1 + zahl2;  
		
		System.out.println("Das Ergebnis ist: " + ergebnis);
		
		/* andere Ausgabe: 
		System.out.println("Das Ergebnis ist: ");
		System.out.println(zahl 1 + " + " + zahl2 + " = " + ergebnis);
		 */
		
		sc.close();  //der Scanner muss geschlossen werden

	}
}
