
public class Arrays {
	public static void main(String args[]) {
		
	  int [] myArray = new int[4];  // a)  3P
	  int a = 3;
	  
	  myArray[0] = 13;              // b)  2P  
	  myArray[1] = 7;
	  myArray[2] = 0;
	  myArray[3] = 2;
	  
	  // 13 - 7 - 0 - 2 - 0         // c) 1P
	  
	  for (int i = 0; i< myArray.length; i++) 
		  System.out.println("Index " + i + ": " + myArray[i]);  // d)  4 P
	  
	  
	  for (int i = 0; i< myArray.length; i++)  // e)  2 P
	    myArray[i] += 5;
	  
	  
	  for (int i = 0; i< myArray.length; i++) {  // f)  2 P
		  myArray[i] += a;
		  a+= 2;
	  }
	  
	  for (int i = 0; i< myArray.length; i++) 
		  System.out.println("Index " + i + ": " + myArray[i]);  
	  
		  // 21 - 17 - 12 - 16    g) 1 P
		  
	  
	//Elemente um 1 nach rechts verschieben
	  for (int i=myArray.length-1; i > 0; i--) {

		  myArray[i]=myArray[i-1] ;

		  }
	  for (int i = 0; i< myArray.length; i++) 
		  System.out.println("Index " + i + ": " + myArray[i]);  
		 
	}

}
